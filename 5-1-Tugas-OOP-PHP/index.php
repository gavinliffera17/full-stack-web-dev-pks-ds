<?php

/**
 * Memiliki Property nama, darah dengan nilai default 50, jumlahKaki, dan keahlian.
 */
trait Hewan
{
    public static $name;
    public $darah = 50,
        $jumlahKaki,
        $keahlian;

    public function __construct($jumlahKaki)
    {
        $this->jumlahKaki = $jumlahKaki;
        echo "Jumlah Kaki : " . $jumlahKaki;
    }

    // di dalam method ini akan menampilkan string nama dan keahlian
    public function atraksi($name, $keahlian)
    {
        echo $name . " sedang " . $keahlian;
    }
}

/**
 * 
 */
trait Fight
{
    public $attackPower, // Serangan
        $defensePower;  // Mempertahankan 

    public function __construct($attackPower, $defensePower)
    {
        $this->attackPower = $attackPower;
        $this->defencePower = $defensePower;
        echo "attackPower = " . $attackPower . " defensePower = " . $defensePower;
    }

    // di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang menyerang elang_3" atau "elang_3 sedang menyerang harimau_2".
    public function serang(Hewan $hewan)
    {
    }

    // di dalam method ini akan menampilkan string sebagai contoh berikut. Contoh : "harimau_1 sedang diserang" atau "elang_3 sedang diserang", kemudian hewan yang diserang akan berkurang darahnya dengan rumus :
    // "darah sekarang - attackPower penyerang / defencePower yang diserang"
    public function diserang()
    {
    }
}

class Elang
{
    use Hewan;
    use Fight;
    //didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau)
    public function getInfoHewan(Hewan $hewan, Fight $fight)
    {
        $this->$elang = new Elang();
        $elang->serang();

        return $elang;
    }
}

class Harimau
{
    use Hewan, Fight;
    //didalam method ini semua property yang ada di dalam kelas Hewan dan Fight ditampilkan , dan jenis hewan (Elang atau Harimau)
    public function getInfoHewan(Hewan $hewan, Fight $fight)
    {
        $harimau = "";
        return $harimau;
    }
}
